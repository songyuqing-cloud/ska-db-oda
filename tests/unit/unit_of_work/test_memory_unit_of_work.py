"""
This module contains tests for MemoryUnitOfWork
"""

import copy

import pytest
from ska_oso_pdm.entities.low.sb_definition import LowSBDefinition
from ska_oso_pdm.schemas import CODEC

from ska_db_oda.repository.memoryrepository import MemoryRepository
from ska_db_oda.unit_of_work.memoryunitofwork import MemoryUnitOfWork


@pytest.fixture
def low_sbi():
    """Load a LOW scheduling block definition from file via CODEC"""
    path = "tests/unit/testfile_sample_low_sb.json"
    scheduling_block = CODEC.load_from_file(LowSBDefinition, path)
    return scheduling_block


def test_add_with_rollback_leaves_repo_unchanged(low_sbi):
    """Adds an SB but do not commit it, confirming that this does not affect
    the repository state"""

    muow = MemoryUnitOfWork(MemoryRepository())
    repo_len_before = len(muow.sbis)

    with muow as session:
        # sanity checks for test setup
        tx_len_before = len(muow._transactions)
        assert tx_len_before == 0, "pending transaction present for new session"
        assert (
            low_sbi not in muow._transactions
        ), "Error in test setup: test SB already in transactions"

        session.add(low_sbi)
        tx_len_during = len(muow._transactions)
        assert tx_len_during == 1, "add not recorded as pending transaction"
        assert (
            low_sbi in muow._transactions
        ), "entity not recorded as pending transaction"

        # no session.commit() so transaction is rolled back

    tx_len_after = len(muow._transactions)
    assert tx_len_after == 0, "pending transactions not cleared after rollback"

    repo_len_after = len(muow.sbis)
    assert repo_len_after == repo_len_before, "repo size affected by uncommitted add"
    assert low_sbi.sbi_id not in muow.sbis, "uncommitted SB added to repo"


def test_add_with_commit_modifies_repo(low_sbi):
    """Add two SBIs, one committed and another uncommitted, and check that the
    repository has the expecgted state.
    """

    # make a copy of the test SB. This SB will be added but not committed.
    uncommitted_sb = copy.deepcopy(low_sbi)
    uncommitted_sb.sbi_id = "foo"

    muow = MemoryUnitOfWork(MemoryRepository())
    repo_len_before = len(muow.sbis)

    with muow as session:
        session.add(low_sbi)
        session.commit()
        session.add(uncommitted_sb)
        # no session.commit() so this last SB is uncommitted

    repo_len_after = len(muow.sbis)

    # SBI dict change is permanent, transaction state is cleared (can't be rolled back)
    assert low_sbi.sbi_id in muow.sbis, "committed SB not added to repo"
    assert uncommitted_sb.sbi_id not in muow.sbis, "uncommitted SB added to repo"
    assert (
        repo_len_after == repo_len_before + 1
    ), "committed addition did not increase repo size"
    assert len(muow._transactions) == 0, "transactions recorded as still pending"


def test_get(low_sbi):
    """Retrieves committed SBI and verifies it is the same as the one stored"""
    repo = MemoryRepository()
    repo.add(low_sbi)
    muow = MemoryUnitOfWork(repo)
    assert muow.get(low_sbi.sbi_id) == low_sbi
