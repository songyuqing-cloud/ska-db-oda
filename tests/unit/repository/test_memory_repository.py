"""
This module contains tests for MemoryRepository.
"""

import pytest
from ska_oso_pdm.entities.low.sb_definition import LowSBDefinition
from ska_oso_pdm.schemas import CODEC

from ska_db_oda.repository.memoryrepository import MemoryRepository


@pytest.fixture
def low_sbi():
    """Load a LOW scheduling block definition from file via CODEC"""
    path = "tests/unit/testfile_sample_low_sb.json"
    scheduling_block = CODEC.load_from_file(LowSBDefinition, path)
    return scheduling_block


def test_add(low_sbi):
    """Adds a json object (mock sbi) to the MR
    Verifies via length and ID of stored mock SBI
    """
    mr = MemoryRepository()
    len_before = len(mr._sbi_dict)
    mr.add(low_sbi)
    len_after = len(mr._sbi_dict)
    assert len_after == len_before + 1 and low_sbi.sbi_id in mr._sbi_dict


def test_get_returns_sb_with_expected_id(low_sbi):
    """Retrieves mock SBI and verifies it is the same as the one stored"""
    mr = MemoryRepository()
    mr.add(low_sbi)

    assert mr.get(low_sbi.sbi_id) == low_sbi


def test_len(low_sbi):
    """
    Verify that the length of a MemoryRepository is correctly returned as the
    number of sbs contained in the repository.
    """
    repo = MemoryRepository()
    assert len(repo) == 0
    repo.add(low_sbi)
    assert len(repo) == 1


def test_contains(low_sbi):
    """
    Verify that testing inclusion in a MemoryRepository checks whether an SB
    with the given ID is contained by the repository.
    """
    repo = MemoryRepository()
    assert low_sbi.sbi_id not in repo
    repo.add(low_sbi)
    assert low_sbi.sbi_id in repo
