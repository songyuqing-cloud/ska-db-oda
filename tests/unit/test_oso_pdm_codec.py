"""
This module contains tests for ska_oso_pdm.schemas.CODEC

To explore the JSON de/serialisation functionality
"""

import json

from ska_oso_pdm.entities.low.sb_definition import LowSBDefinition
from ska_oso_pdm.schemas import CODEC


def test_load_from_file():
    """Load a LOW scheduling block definition from file via CODEC"""
    path = "tests/unit/testfile_sample_low_sb.json"
    scheduling_block = CODEC.load_from_file(LowSBDefinition, path)
    assert scheduling_block is not None


def test_load_from_file_verify_id():
    """Verifies scheduling block id from loaded JSON file"""
    path = "tests/unit/testfile_sample_low_sb.json"
    scheduling_block = CODEC.load_from_file(LowSBDefinition, path)
    assert (
        scheduling_block.sbi_id == "sbi-mvp01-20200325-00001"
    ), f"debug, extracted id is: {scheduling_block.sbi_id}"


def test_oso_pdm_serialisation():
    """
    Serialises a scheduling block object into a json string
    and asserts the keys of the JSON
    """

    path = "tests/unit/testfile_sample_low_sb.json"
    scheduling_block = CODEC.load_from_file(LowSBDefinition, path)

    sb_json_string = CODEC.dumps(scheduling_block)
    sb_json = json.loads(sb_json_string)

    assert set(sb_json.keys()) == {
        "field_configurations",
        "mccs_allocation",
        "sbi_id",
        "scan_definitions",
        "scan_sequence",
        "subarray_beam_configurations",
        "target_beam_configurations",
    }, f"debug, len: {len(sb_json.keys())}, keys: {len(list(sb_json.keys()))}"
