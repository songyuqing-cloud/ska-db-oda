# SKA Database OSO Data Archive

The repository for the SKA’s ‘Observatory Science Operations’ (OSO) Data Archive

## Project Description
This project contains code for the Data Archive and accompanying client library. The Data Archive is a prototype JSON document store for the SKA’s ‘Observatory Science Operations’ (OSO) subsystem.

## Quickstart

This project is structured to use Docker containers for development and
testing so that the build environment, test environment and test results are
all completely reproducible and are independent of host environment. It uses
``make`` to provide a consistent TUI.

Build a new Docker image :

```
make build
```

Execute the test suite with:
```
make test
```

Launch an interactive shell inside a container, with your workspace visible
inside the container:

```
make interactive
```

[![Documentation Status](https://readthedocs.org/projects/ska-db-oda/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-db-oda/en/latest/?badge=latest)

Documentation can be found in the ``docs`` folder.
