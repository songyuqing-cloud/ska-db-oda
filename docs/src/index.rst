================================
SKA ODA Prototype documentation
================================

This project is developing the Observatory Science Operations Data Archive
prototype for the `Square Kilometre Array`_.

.. _Square Kilometre Array: https://skatelescope.org/

.. toctree::
    :maxdepth: 1
    :caption: Repository

    repository/MemoryRepository
    repository/AbstractRepository


.. REFERENCE SECTION =============================================================

.. toctree::
    :maxdepth: 1
    :caption: Reference material

    reference/Repository_Class_Diagram
    reference/Unit_Of_Work_Class_Diagram
    reference/Overview


.. README =============================================================

.. .. toctree::
..     :maxdepth: 2
..     :caption: README.md

..      ../README.md

.. COMMUNITY SECTION =============================================================


Indices and tables
------------------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
