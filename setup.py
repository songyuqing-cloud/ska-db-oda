#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

import setuptools

# prevent unnecessary installation of pytest-runner
needs_pytest = {"pytest", "test", "ptr"}.intersection(sys.argv)
pytest_runner = ["pytest-runner"] if needs_pytest else []


with open('README.md', encoding="utf-8") as readme_file:
    readme = readme_file.read()

setuptools.setup(
    name='ska-db-oda',
    version="0.2.0",
    description="OSO Data Archive",
    long_description=readme + '\n\n',
    author="Humzah Javid",
    author_email='humzah.javid@stfc.ac.uk',
    url='https://gitlab.com/ska-telescope/ska-db-oda',
    packages=setuptools.find_namespace_packages(where='src'),
    package_dir={'': 'src'},
    include_package_data=True,
    license="BSD license",
    zip_safe=False,
    keywords='ska-db-oda, ska_database_oso_data_archive, db-oda, oso, data, archive',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
    ],
    install_requires=[
        "ska-oso-pdm>=7.1.0",
        "ska-ser-logging"
    ],
    setup_requires=[
        # dependencies for `python setup.py build_sphinx`
        'sphinx',
    ] + pytest_runner,
    # some test libs not needed (like fork)
    tests_require=[
        'pytest',
        'pytest-cov',
        'pytest-forked',
        'pytest-json-report',
        'pycodestyle',
    ],
    extras_require={
        'dev':  ['prospector[with_pyroma]', 'yapf', 'isort']

    },
    dependency_links=[
    ]

)
