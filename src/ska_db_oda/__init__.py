"""
This package implements SKA OSO Data Archive
"""
from ska_ser_logging import configure_logging

configure_logging()
