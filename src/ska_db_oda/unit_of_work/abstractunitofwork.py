"""
This module contains the UnitOfWork Abstract Base Class.

All UnitOfWork implementations to conform to this interface.
"""

from abc import abstractmethod

from ska_db_oda.repository.abstractrepository import AbstractRepository


class AbstractUnitOfWork(AbstractRepository):
    """Provides the interface to store or retrieve a group of Scheduling Block Objects.

    Commits or rollsback a series of database transactions as an atomic operation
    """

    sbis: AbstractRepository

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.rollback()

    @abstractmethod
    def commit(self):
        """Commits the Unit of Work."""
        raise NotImplementedError

    @abstractmethod
    def rollback(self):
        """Initiates the rollback of this Unit of Work.

        If no commit is carried out or an error is raised,
        the unit of work is rolled back to a safe state
        """
        raise NotImplementedError
