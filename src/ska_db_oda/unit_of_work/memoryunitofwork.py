"""
This module contains the implementation of the AbstractUnitOfWork Class.
"""

import logging
from typing import Optional, Union

from ska_oso_pdm.entities.common import SBDefinitionID
from ska_oso_pdm.entities.low.sb_definition import LowSBDefinition
from ska_oso_pdm.entities.mid.sb_definition import SBDefinition

from ska_db_oda.repository.memoryrepository import MemoryRepository
from ska_db_oda.unit_of_work.abstractunitofwork import AbstractUnitOfWork

LOGGER = logging.getLogger(__name__)


class MemoryUnitOfWork(AbstractUnitOfWork):
    """
    A lightweight non-persistent implementation of the AbstractUnitOfWork that
    can store and retrieve SchedulingBlock objects.

    Commits or rollsback a series of database transactions as an atomic operation.
    Changes between commits are tracked via the _transactions variable.
    """

    def __init__(self, repo: Optional[MemoryRepository] = MemoryRepository()) -> None:
        self.sbis = repo
        self.committed = False
        # the transactions list
        self._transactions = []

    def __enter__(self) -> None:
        return super().__enter__()

    def __exit__(self, *args) -> None:
        self.rollback()

    def commit(self):
        """Commits the Unit of Work (transactions). Rolling back after this point
        is not possible, due to clearing the transaction state."""
        LOGGER.info(
            "Committing %s SBI additions/modifications", len(self._transactions)
        )
        while self._transactions:
            temp_sbi = self._transactions.pop()
            self.sbis.add(temp_sbi)
        # commit was successful, clear the transaction (again for peace of mind)
        self._transactions = []
        self.committed = True

    def rollback(self):
        """Initiates the rollback of this Unit of Work.

        The SBIs that were added to the transactions are removed.
        """
        # clear the state of changes
        LOGGER.info(
            "Rolling back %s pending SBI additions/modifications",
            len(self._transactions),
        )
        self._transactions = []

    def add(self, sbi: Union[SBDefinition, LowSBDefinition]) -> None:
        """Add sbi transactions list to keep track of changes for potential commit

        :param sbi: Scheduling Block Instance
        :type sbi: Union[SBDefinition, LowSBDefinition]
        """

        self._transactions.append(sbi)

    def get(self, sbi_id: SBDefinitionID) -> Union[SBDefinition, LowSBDefinition]:
        """Retrieves a Scheduling Block Instance via an sbi_id.

        :param sbi_id: Contains a unique idenitifer for a Scheduling Block
        :type sbi_id: SBDefinitionID
        :return: Scheduling Block Instance
        :rtype: Union[SBDefinition, LowSBDefinition]
        """

        return self.sbis.get(sbi_id)
