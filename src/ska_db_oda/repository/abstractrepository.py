"""
This module contains the repository Abstract Base Class.

For all Repository implementations to conform to this interface.
"""

from abc import ABC, abstractmethod
from typing import Union

from ska_oso_pdm.entities.common import SBDefinitionID
from ska_oso_pdm.entities.low.sb_definition import LowSBDefinition
from ska_oso_pdm.entities.mid.sb_definition import SBDefinition


class AbstractRepository(ABC):
    """
    Provides the interface to store or retrieve a Scheduling Block Instances.
    """

    @abstractmethod
    def add(self, sbi: Union[SBDefinition, LowSBDefinition]) -> None:
        """Stores a Scheduling Block Instance

        :param sbi: Scheduling Block Instance
        :type sbi: Union[SBDefinition, LowSBDefinition]
        :raises NotImplementedError: add not implemented
        """
        raise NotImplementedError

    @abstractmethod
    def get(self, sbi_id: SBDefinitionID) -> Union[SBDefinition, LowSBDefinition]:
        """Retrieves a Scheduling Block Instance via an sbi_id

        :param sbi_id: Contains a unique idenitifer for a Scheduling Block
        :type sbi_id: SBDefinitionID
        :raises NotImplementedError: get not implemented
        :return: Scheduling Block Instance
        :rtype: Union[SBDefinition, LowSBDefinition]
        """
        raise NotImplementedError
