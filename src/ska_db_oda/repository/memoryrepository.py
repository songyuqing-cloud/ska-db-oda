"""
This module contains implementation of the AbstractRepository class.
"""

import logging
from typing import Dict, Union

from ska_oso_pdm.entities.common import SBDefinitionID
from ska_oso_pdm.entities.low.sb_definition import LowSBDefinition
from ska_oso_pdm.entities.mid.sb_definition import SBDefinition

from ska_db_oda.repository.abstractrepository import AbstractRepository

LOGGER = logging.getLogger(__name__)


class MemoryRepository(AbstractRepository):
    """
    A lightweight non-persistent implementation of the ODA for development and tests
    thus not requiring a heavyweight database implementation
    """

    def __init__(self):
        self._sbi_dict: Dict[SBDefinitionID, Union[SBDefinition, LowSBDefinition]] = {}

    def __len__(self):
        return len(self._sbi_dict)

    def __contains__(self, item):
        if item in self._sbi_dict:
            return True
        else:
            return False

    def add(self, sbi: Union[SBDefinition, LowSBDefinition]) -> None:
        """Stores a Scheduling Block Instance

        :param sbi: Scheduling Block Instance
        :type sbi: Union[SBDefinition, LowSBDefinition]
        """
        sbi_id = sbi.sbi_id
        LOGGER.info("Storing SBI %s", sbi_id)
        # SB string representation is large and will pollute logs, so don't
        # log them unless absolutely necessary
        LOGGER.debug("SBI %s = %s", sbi_id, sbi)
        self._sbi_dict[sbi_id] = sbi

    def get(self, sbi_id: SBDefinitionID) -> Union[SBDefinition, LowSBDefinition]:
        """Retrieves a Scheduling Block Instance via an sbi_id

        :param sbi_id: Contains a unique idenitifer for a Scheduling Block
        :type sbi_id: SBDefinitionID
        :return: Scheduling Block Instance
        :rtype: Union[SBDefinition, LowSBDefinition]
        """
        LOGGER.info("Retrieving SBI %s", sbi_id)
        return self._sbi_dict[sbi_id]
